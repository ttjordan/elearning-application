<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|---------------------å-----------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function()
{
    return View::make('pages.landing');
});

//Route::resource('downtime', 'DownTimeController');

// Private Pages
//Route::get('users', 'UsersController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/profile', 'ProfileController');
Route::resource('users', 'UsersController');
Route::resource('courses', 'CoursesController');
Route::resource('modules', 'ModulesController');
Route::resource('slides', 'SlidesController');
Route::resource('enrollment', 'EnrollmentController');
//Route::resource('enrollment_status', 'Enrollment_statusController');
//Route::resource('role', 'RoleController');
//Route::resource('permission', 'PermissionController');
//Route::resource('permission_role', 'Permission_roleController');

Route::get('users/dashboard', 'UsersController@dashboard')->name('users.dashboard');;