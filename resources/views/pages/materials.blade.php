@extends('layouts.default-public')
@section('content')

<div class="application-materials-background">
	<div class="container">
		<!-- Heading Row -->
		<div class="row">

			<div class="col-xs-12">
				<h1 class="page-title">
					CompSci University Presents:<br>
					<strong>Web Development 101</strong>
				</h1>
			</div>
			<div class="col-md-4 col-md-offset-4 center-logo">
				{{ HTML::image('/assets/images/web-logos.png', 'web logos', array('class' => 'img-responsive') ) }}
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 well well-lg materials-intro-text">
				<h2>Introduction {{ HTML::image('/assets/images/Dots.png') }}</h2>
				<p>Would you like to learn the basics of web development? After taking this course you will be familiar with:</p>
				<ul>
					<li>Stylizing webpages with <strong>CSS</strong>.</li>
					<li>Creating layouts with <strong>HTML</strong>.</li>
					<li>Animating content with <strong>JavaScript</strong>.</li>
				</ul>
				
				<p>There is no cost, but we do ask for your commitment of time and interest.</p>
				
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 well well-lg">
				<h2>Resources {{ HTML::image('/assets/images/Dots.png') }}</h2>
				<div class="col-xs-12">
					<div class="col-xs-2 adobe-icon materials-icons"><a href="/assets/documents/project_LIFT_info_sheet.pdf" target="_blank">{{ HTML::image('/assets/images/PDF Icon.png') }}</a></div>
					<div class="col-xs-9"><a href="#" target="_blank"> <p class="materials-2nd-well-text"></p> Download the <strong>course sylabus.</strong> </a></div>
				</div>	
				<div class="col-xs-12">
					<div class="col-xs-2 form-icon materials-icons"><a href="/assets/documents/applicant_form.docx" target="_blank">{{ HTML::image('/assets/images/Form Icon.png') }}</a></div>
					<div class="col-xs-9"><a href="#" target="_blank"><p class="materials-2nd-well-text"></p>Fill out the <strong>applicant form.</strong> </a></div>
				</div>	
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 well">
				<h2>Frequently Asked Questions {{ HTML::image('/assets/images/Dots.png') }}</h2>
				<div class="panel-group" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse-1">
							<h4 class="panel-title">What can I learn in this program?</h4>
							</a> 
						</div>
						<div id="collapse-1" class="panel-collapse collapse in">
							<div class="panel-body">
								<p>Loren sump dolor sit mate, unique philosophic pro at. Us doctors time am error ibis no. I man quad exercise, populous possum instructor in me. Eu wist intelligent tied, acquit per sues rip ides emu in. Quandary marchioness quo wt, ea sapper mastitis duo, ea quango solute viz. In pro quins consequent, denim fastidious copious quo ad. Usu it mollies nostrum, per en appropriate negligent. Me sol eat ancillary en. </p>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse-2">
							<h4 class="panel-title">Do I need to know how to code to take this course?</h4>
							</a> 
						</div>
						<div id="collapse-2" class="panel-collapse collapse">
							<div class="panel-body">
								<p>Eu wist intelligent tied, acquit per sues rip ides emu in. Id mes mover elect ram assertion has no. Me sumo unique argument um no. Moro om ques cu bis, is ex male rum squamous it up ration. Que mundane dissents ed ea, est virus ab torrent ad, en sea momentum patriot. Usu ilia accusatory constitute e, in anti map salutation viz, at snit kumquat epicure cum. Que tongue mutant at, peter chum asseverate en pro. </p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

  
@stop