@extends('layout')


@section('sidebar')
	{!! $child !!}
@endsection


@section('content')


<!-- start page content -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="row x_title">
                    <div class="col-md-12">

                        <h3>{{ $slides->name }}

						<div class="pull-right">
								<a href="/slides/{{ $slides->id }}/edit" class="btn btn-primary"><i class="fa fa-wrench"></i> Edit Slide</a> 

								<button class="btn btn-primary" >Transcript:
									<input id="id-button-borders" checked="checked" type="checkbox" class="ace ace-switch ace-switch-3 transcript-btn">
									<span class="lbl middle"></span>
								</button>

							
						</div>

						</h3>
                    </div>
                </div>

                <div class="x_content">
                    <div class="row">

						<!-- VIDEO BLOCK -->
						<div id="player-wrapper" class="col-md-10">
							{!! $slides->media_script != '' ? '<div id="player" class="youtube" style="width:100%; height:auto" ></div>' : ''  !!}
						</div>

						<!-- TRANSCRIPT BLOCK -->
						<div id="transcript" class="col-md-2">
							<p>{{ $slides->transcript }}</p>
						</div>	

						<?php
						// NEED TO PUT IN CONTROLLER!!!!
						$id = $slides->modules->id;

						// TODO PUT IN CONTROLLER, this function gets the total of slides 
						// that belogs to this group of slides/moduel then store them for NEXT & PREV
						foreach($allModSlides as $slide){
							$items[] = $slide->id;
						}

						// Find the index of the current item
						$current_index = array_search($slide->id, $items);

						// Find the index of the next/prev items
						$next = $current_index + 1;
						$prev = $current_index - 1;	

						//var_dump($slide->id);
						//var_dump($items);
						//var_dump($current_index);
						//dd($prev . '  ' . $next);
						?>

						<!-- QUIZ BLOCK -->
						<div id="show-quiz" class="col-xs-12 " style="display:none;">
							<div class="widget-box">	
								<div class="widget-main"></div>
							</div>
						</div>


						<!-- POP UP Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Test Your Knowledge</h4>
									</div>
									<div class="modal-body">
										<div><strong id="q"></strong></div>
										<div class="space-6"></div>
										<form id="o"></form>
									</div>
									<div class="modal-footer">

										<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
										<button class="btn btn-primary" onclick="getAnswer();">Get Answer</button>

									</div>
								</div>
							</div>
						</div>

						<!-- BOTTOM BLOCK  -->
						<div class="col-md-8">

							<h3 class="header blue lighter smaller">
								<i class="fa fa-angle-double-left"></i>
								<i class="fa fa-angle-double-right"></i>
								Slide navigation
							</h3>

							<div class="pull-left">
								<button href="#" id="quiz-button-popup" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="display:none; margin: 0px ">Show Quiz</button>

								<?php if ($prev > - 1){ echo  '<a class="btn btn-primary" href="/slides/'. $items[$prev] .'" style=" margin: 0px 5px 0px 3px;">Previous slide</a>';} 
									?>
									
									{{ Form::open(array('action' => 'EnrollmentController@store', 'class'=>'pull-right')) }}
										<input type="hidden" name="nextPage" value="{{ isset($items[$next]) ? $items[$next] : '' }}">
										<input type="hidden" name="module_id" value="{{ $slides->modules->id }}">
										<input type="hidden" name="user_id" value="{{ Auth::id() }}">
										<input type="hidden" name="enrollment_status_id" value="3">
										<input type="hidden" name="slide_id" value="{{ $slides->id }}">

										<div id="next-slide" style="display:none;">
											{{ Form::submit('Next slide', array('class' => 'btn btn-primary')) }}
										</div>
									{{ Form::close() }}
							</div>	
						
						</div>

						<div class="col-md-4" style="padding-top: 15px;">

							<!-- STATS BLOCK -->
							<?php 
								// get slides that where completed

								// get remaining slide
								$slidesRemaining = $allSlides->slides->count() - $slidesCompleted;
								
								//dd($slidesCompleted);
								// divide completed by slide count
								$percent =  $slidesCompleted / $allSlides->slides->count(); 
								// the percent will have  decimal, only need part of that
								$percentformat = number_format((float)$percent, 2, '', '');
								// need to return part of the string
								$percentStr = substr($percentformat, 1);

								// if all the slides are done then, change the variable 100%
								if($allSlides->slides->count() == $slidesCompleted){
									$percentStr = 100;
								}
							?>



								<div class="pull-right" style="width: 50%;">
									<div class="infobox-progress">
										<div style="text-align: center; margin-bottom: 17px">
											<span class="chart" data-percent="{{ $percentStr }}">
												<span class="percent">{{ $percentStr }}</span>
												<canvas height="220" width="220" style="height: 110px; width: 110px;"></canvas>
											</span>
										</div>
									</div>
								</div>

								<div class="pull-right" style="width: 50%;">						
									<ul class="list-inline widget_tally">
									<li>
										<p>
											{{ 'Total Slides: ' . $allSlides->slides->count() }}
										</p>
									</li>

									<li>
										<p>
											{{ 'Slides Remaining:'. $slidesRemaining }}
										</p>
									</li>

									<li>
										<p>
											{{ 'Completed:'. $slidesCompleted }}
										</p>
									</li>
									</ul>
								</div>

					
						</div>

					</div>
            </div>
        </div>

    </div>
</div>
<!-- /end page content -->
@stop() 


<!-- PUT LINK TO SCRIPT HERE / BELOW CODE WILL GO INSIDE A CUSTOM JS FILE -->

@section('scripts')
<!-- YouTube API load script -->					
<script src="https://www.youtube.com/player_api"></script>

<!-- page specific plugin scripts -->
<script src="/assets/js/jquery.easypiechart.js"></script>



<script>

//init variables 
var obj_ques = <?php echo json_encode($question); ?> //check if json set
var obj_opt  = <?php echo json_encode($answers); ?> //check if json set
var options = '';
var obj_opt2 = obj_opt != null ? obj_opt : ''; // convert string back in array object

$(function() {

	//check if obj_opt variable is set
	if(obj_opt != null){

		for(var i = 0; i < obj_opt2.a.length; i++)
		{
			options += '<label><input id="'+ i +'" type="radio" class="check-answer" name="answer" value="' + i +'"> ' + obj_opt2.a[i].option + '</label><br>';
		}
		// Add html input to DOM div tag id="a"
		$("#q").html(obj_ques);
		$("#o").html(options);

	}
});

function getAnswer(){

	//init variables  
	var obj_opt  = <?php echo json_encode($answers); ?> //check if json set
	var obj_check = $('#o').find('input');
	var obj_opt2 = obj_opt; // convert string back in array object

	// loop through and find checked option
	$.each( obj_check, function( key, value ) {
	  if(value.checked){
		if(this.id == obj_opt2.checked){
			alert('Correct!');
			$('#next-slide').show();
			$('#myModal').modal('hide');
		}else{
			alert('Sorry, the answer: ' + obj_opt2.a[obj_opt2.checked].option);
			$('#next-slide').show();
			$('#myModal').modal('hide');
		}
	  }
	});
}

    
// create youtube player
var player;
function onYouTubePlayerAPIReady() {
	player = new YT.Player('player', {
	  videoId: '{{ $slides->media_script }}',
	  playerVars: {rel: 0},
	  events: {
		'onReady': onPlayerReady,
		'onStateChange': onPlayerStateChange
	  }
	});
}


var h = window.innerHeight - 290;
$('.youtube, #transcript').css('height', h + 'px');

$( window ).resize(function() {
	var h = window.innerHeight - 290;
	$('.youtube, #transcript').css('height', h + 'px');
});


// autoplay video
function onPlayerReady(event) {
	event.target.playVideo();
}

// when video ends
function onPlayerStateChange(event) {        
	if(event.data === 0) {            
	   // alert('done');
	   //$('#show-quiz').show();
	   if(obj_ques == ''){
		   $('#next-slide').show();
	   }else{
		   $('#myModal').modal('show');
		   $('#quiz-button-popup').show();
	   }

	}
}

$( ".transcript-btn" ).click(function() {

	$( "#transcript" ).toggle();
	
	if($('#player-wrapper').is('.col-md-10')){
		$('#player-wrapper').removeClass('col-md-10')
		$('#player-wrapper').addClass('col-md-12')
	}else{
		$('#player-wrapper').addClass('col-md-10')
		$('#player-wrapper').removeClass('col-md-12')
	}
});
  
</script>

@stop 