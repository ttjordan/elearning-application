@extends('layouts.default')
@section('content')

<a class="btn btn-primary pull-right" href="{{ URL::to('slides/create') }}">Create new Slide</a>

<h1 class="header smaller lighter blue">All the Slides</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Menu Name</td>
			<td>Belongs To Module:</td>
			<td class="col-md-5">Actions</td>
		</tr>
	</thead>
	<tbody>
	@foreach($slides as $key => $value)
		<tr>
			<td>{{ $value->id }}</td>
			<td>{{ $value->name }}</td>
			<td>{{ $value->menu_name }}</td>
			<td>{{ isset($value->module_id) ? $value->modules->name : link_to('slides/'. $value->id.'/edit' , 'No assign module')  }}</td>

			<!-- we will also add show, edit, and delete buttons -->
			<td class="center">

				<!-- show the slide (uses the show method found at GET /slides/{id} -->
				<!--<a class="btn btn-small btn-success" href="{{ URL::to('slides/' . $value->id) }}">Show this Slide</a>-->

				<!-- edit this slide (uses the edit method found at GET /slides/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('slides/' . $value->id . '/edit') }}">Edit this Slide</a>

				<!-- delete the slide (uses the destroy method DESTROY /slides/{id} -->
				<!-- we will add this later since its a little more complicated than the first two buttons -->
				{{ Form::open(array('url' => 'slides/' . $value->id, 'style' => 'display: inline-block')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete this Slide', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
				
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


@stop 