@extends('layouts.default')
@section('content')


<h1 class="header smaller lighter blue">Edit {{ $slide->name }}
	<div class="pull-right">

		<a href="/slides" class="btn btn-primary"><i class="fa fa-chevron-left"></i> All Slides</a> 
		<a href="/slides/{{ $slide->id }}" class="btn btn-primary"><i class="fa fa-wrench"></i> Preview Slide</a> 
		<a href="{{ URL::to('slides/create') }}" class=" btn btn-primary" > Create New Slide </a> 
	</div>
</h1>

<!-- if there are creation errors, they will show here -->
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

{{ Form::model($slide, array('action' => array('SlidesController@update', $slide->id), 'method' => 'PUT', 'files'=>true)) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('name', 'Slide Name:') }}
				{{ Form::text('name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('menu_name', 'Menu Name:') }}
				{{ Form::text('menu_name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('description', 'Description:') }}
				{{ Form::textarea('description', null, array('id' => 'description-input', 'class' => 'form-control', 'onKeyPress' => 'check_length(this.form);', 'onKeyDown' => 'check_length(this.form);'  )) }}
				<input size="3" value="140" name="text_num" style="margin-top:5px;"> Characters Left </input>
			</div>
		</div>
		
		<div class="col-xs-12 col-md-5">
			<div class="form-group">
				{{ Form::label('media_script', 'Media Script:') }}
				{{ Form::text('media_script', null, array('class' => 'form-control')) }}
			</div>	
		</div>	

		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('transcript', 'Transcript:') }}
				{{ Form::textarea('transcript', null, array('id' => 'transcript-input', 'class' => 'form-control' )) }}
			</div>
		</div>
		
		<div class="col-xs-12 col-md-4">
			<div class="form-group">
				{{ Form::label('module_id', 'Select Module:') }}
				{{ Form::select('module_id',  $module_list, old('module_id'), array('class' => 'form-control')) }}

			</div>
		</div>


		<div class="col-xs-12">
			Do you want to add a question?<br />
			<a id="show-quiz">Yes</a> | <a id="hide-quiz" >No</a>
			
			<fieldset class="new-quesiton-wrapper scheduler-border" style="display: none;">
			<legend class="scheduler-border">Quiz Question</legend>
				
				<div id="new-quesiton-box">
				 
					 <div class="form-group">
						{{ Form::label('question', 'Question:') }}
						{{ Form::text('question', null, array('class' => 'question form-control', 'placeholder' => 'Input Question' )) }}
					 </div>
					 
					<hr>
					
					<div class="form-group">
						<a id="addOpt" class="btn btn-sm btn-primary">Add another option</a>
					</div>	
					 
					<div id="p_scents">
				
					</div>

					 <input type="hidden" name="answers" id="submission-question">
				 
				</div>
				
			</fieldset>
			<br /><br />	
			
			@if(isset($slide->fileentry->filename))
			<hr>
			<img src="/assets/uploads/{{ $slide->fileentry->filename }}">
			
			@endif
		</div>

		<div class="col-xs-12">	
			<div class="form-group">
				<div class="control-group">
					<div class="controls">
					{{ Form::label('image', 'Upload image') }}
					{{ Form::file('image') }}
					  <p class="errors">{{ $errors->first('image')}}</p>
						@if(Session::has('error'))
						<p class="errors">{{ Session::get('error') }}</p>
						@endif
					</div>
				</div>
				<div id="success"> </div>
			</div>
			<hr>
			<input id="send" class="btn btn-primary" type="submit" value="Submit">
		</div>
	</div>
{{ Form::close() }}


@stop


<!-- PUT LINK TO SCRIPT HERE / BELOW CODE WILL GO INSIDE A CUSTOM JS FILE -->
@section('scripts')
<script>

/***********************************************************************************************
* 
***********************************************************************************************/
$(function() {

	$('#media_script').wrap('<div class="input-group"></div>');
	$('#media_script').before('<div class="input-group-addon">https://www.youtube.com/watch?v=</div>');

	//init variables 
	var obj_opt  = <?php echo json_encode($answers); ?> //check if json set
	var options = "";
	var obj_opt2 = obj_opt == ''? '' : obj_opt; // convert string back in array object

	//check if obj_opt variable is set
	if(obj_opt2 != null){
		
		for(var i = 0; i < obj_opt2.a.length; i++)
		{
		options +=  '<p><label for="p_Opts">Option:</label> <input id="'+ i +'" class="correct" type="radio" name="correct"> <input type="text" class="option form-control" placeholder="Input Value" value="'+ obj_opt2.a[i].option +'" > <a href="#" id="remOpt">Remove</a></p>'
		}
		// Add html input to DOM div tag id="a"
		$("#p_scents").html(options);

		//check radio button
		var id = obj_opt2.checked;
		$('#'+ id).prop( "checked", true );
	}	
});

/***********************************************************************************************
* 
***********************************************************************************************/
$(function() {
	//init variables 
	var obj_opt  = <?php echo json_encode($answers); ?> //check if json set
	var obj_opt2 = obj_opt == ''? '' : obj_opt; // convert string back in array object

	//hide quiz wrapper
	$("#hide-quiz").click(function(){
		$(".new-quesiton-wrapper").hide();
		$(".new-quesiton-wrapper input").val('');
		$(".new-quesiton-wrapper input").prop('checked', false);
	});
	
	//show quiz wrapper
	$("#show-quiz").click(function(){
		$(".new-quesiton-wrapper").show();
	});
	
	// init variables
	var scntDiv = $('#p_scents');
	// check if obj_opt is empty string
	var i = obj_opt2 == null ?  $('#p_scents p').size() + 1 : obj_opt2.a.length;
	
	// add another answer option
	$(document).on("click", '#addOpt', function() {
		$('<p><label for="p_Opts">Option: </label> <input id="'+ i +'" class="correct" type="radio" name="correct"> <input type="text" class="option form-control" placeholder="Input Value" /> <a href="#" id="remOpt">Remove</a></p>').appendTo(scntDiv);
		i++;
		return false;
	});//@end
	
	// remove answer option
	$(document).on("click",'#remOpt', function() { 
		if( i >= 1 ) {
			$(this).parents('p').remove();
			i--;
		}
		return false;
	});//@end
	
	//check if obj_opt variable is set
	if(obj_opt2 == ""){
		$("#p_scents").html('<p><label for="p_Opts">Option: </label> <input id="0" class="correct" type="radio" name="correct"> <input type="text" class="option form-control" placeholder="Input Value" />')
	}else{
		$(".new-quesiton-wrapper").show();
	}//@end
	
	//Submit form button and validate input submission
	$("#send").click(function(event) {
		//prevent Default functionality
		event.preventDefault();

		if($('.new-quesiton-wrapper').is(':visible')){
			if($('.question').val() != "" && $("form input[type='radio']:checked").val()){
				$('#submission-question').val( JSON.stringify($('form').serializeObject()) );
				$("form").submit();
			}else{
				alert('Question field required and check one option!');
			}
		}else{
			$("form").submit();
		}
	});	
});

/***********************************************************************************************
* Varaible Funciton
* 
* We loop through the questions then store them in a JSON string.
* During the submisison process above, we set the JSON string object
* to id="submission-question" value will be JSON string.
***********************************************************************************************/
$.fn.serializeObject = function()
{
	// init variables
	var a = $('.option');
	var options = [];
	$.each(a, function(i,v) {
		options.push({option: v.value})
	});
	
	// grab the checked answer
	var checked = $('input[name=correct]:checked').attr('id');
	
	// build the json array object, which will be sent and stored in database.
	var q = {
		"a": options,
		"checked":checked 
	} 

    return q;
};//@end

/***********************************************************************************************
* Description limit count.
***********************************************************************************************/
function check_length(my_form)
{
	var maxLen = 140; // max number of characters allowed

	if (my_form.description.value.length >= maxLen) {
		// Alert message if maximum limit is reached. 
		// If required Alert can be removed. 
		var msg = "You have reached your maximum limit of characters allowed";
		alert(msg);
		// Reached the Maximum length so trim the textarea
		my_form.description.value = my_form.description.value.substring(-1, maxLen);
	 }else{ // Maximum length not reached so update the value of description counter
		my_form.text_num.value = maxLen - my_form.description.value.length;
	}
}

</script>


@stop 