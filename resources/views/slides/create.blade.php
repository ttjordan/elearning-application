@extends('layouts.default')
@section('content')

<a class="btn btn-primary pull-right" href="{{ URL::to('slides/create') }}">Create new Slide</a>

<h1 class="header smaller lighter blue">Create a Slide</h1>

<!-- if there are creation errors, they will show here -->
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

{{ Form::open(array('url' => 'slides', 'files'=>true )) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('name', 'Slide Name:') }}
				{{ Form::text('name', old('name'), array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('menu_name', 'Menu Name:') }}
				{{ Form::text('menu_name', old('menu_name'), array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('description', 'Description:') }}
				{{ Form::textarea('description', null, array('id' => 'description-input', 'class' => 'form-control')) }}
			</div>
		</div>
			
		<div class="col-xs-12 col-md-5">
			<div class="form-group">
				{{ Form::label('media_script', 'Media Script:') }}
				{{ Form::text('media_script', old('media_script'), array('class' => 'form-control')) }}
			</div>
		</div>
			
		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('transcript', 'Transcript:') }}
				{{ Form::textarea('transcript', old('transcript'), array('id' => 'transcript-input', 'class' => 'form-control')) }}
			</div>
		</div>
			


			<div class="col-xs-12">
				Do you want to add a question?<br />
				<a href="#" id="show-quiz">Yes</a> | <a href="#" id="hide-quiz" >No</a>
					
				<fieldset class="new-quesiton-wrapper scheduler-border" style="display:none;">
				<legend class="scheduler-border">Quiz Question</legend>
					
					<div id="new-quesiton-box">
					 
						 <div class="form-group">
							<label>Question:</label><br>
							<input class="question form-control" type="text" name="question" placeholder="Input Question">
						 </div>
						 
						<hr>
						
						<div class="form-group">
							<a href="#" id="addOpt" class="btn btn-sm btn-primary">Add another option</a>
						</div>	
						 
						<div id="p_scents">
							<p>
								<label for="p_Opts">Option:</label>
								<input id="0" class="correct" type="radio" name="correct">
								<input type="text" class="option form-control" placeholder="Input Value" >
								
							</p>
						</div>


						 <input type="hidden" name="answers" id="submission-question">
					 
					</div>
					
				</fieldset>
				<br /><br />	
			</div>

			<div class="col-xs-12">
				<hr>
				<div class="form-group">
					<div class="control-group">
						<div class="controls">
						{{ Form::label('image', 'Upload image') }}
						{{ Form::file('image') }}
						  <p class="errors">{{ $errors->first('image')}}</p>
							@if(Session::has('error'))
							<p class="errors">{{ Session::get('error') }}</p>
							@endif
						</div>
					</div>
					<div id="success"> </div>
				</div>
				<hr>
				{{ Form::submit('Create the Slide!', array('class' => 'btn btn-primary')) }}
			</div>
	</div>
{{ Form::close() }}
@stop
 

@section('scripts')

<script type="text/javascript">

$.fn.serializeObject = function()
{
	// init variables
	var a = $('.option');
	var options = []
	$.each(a, function(i,v) {
		options.push({option: v.value})
	});
	
	// grab the checked answer
	var checked = $('input[name=correct]:checked').attr('id');
	
	// build the json array object, which will be sent and stored in database.
	var q = {
		"a": options,
		"checked":checked 
	} 

    return q;
};

$(function() {
	$('#media_script').wrap('<div class="input-group"></div>');
	$('#media_script').before('<div class="input-group-addon">https://www.youtube.com/watch?v=</div>');
	
	//show quiz wrapper
	$("#hide-quiz").click(function(){
		$(".new-quesiton-wrapper").hide();
		$(".new-quesiton-wrapper input").val('');
		$(".new-quesiton-wrapper input").prop('checked', false);
	});
	$("#show-quiz").click(function(){
		$(".new-quesiton-wrapper").show();
	});
		
	
	//Submit form button
	$('form').submit(function() {
		$('#submission-question').val(JSON.stringify($('form').serializeObject()));
		//console.log(JSON.stringify($('form').serializeObject()));
	});

	// init variables
	var scntDiv = $('#p_scents');
	var i = $('#p_scents p').size() + 0;
	
	// add another answer option
	$(document).on("click", '#addOpt', function() {
		$('<p><label for="p_Opts">Option: </label> <input id="'+ i +'" class="correct" type="radio" name="correct"> <input type="text" class="option form-control" placeholder="Input Value" /> <a href="#" id="remOpt">Remove</a></p>').appendTo(scntDiv);
		i++;
		return false;
	});
	
	// remove answer option
	$(document).on("click",'#remOpt', function() { 
		if( i > 1 ) {
			$(this).parents('p').remove();
			i--;
		}
		return false;
	});
	
});

</script>

@stop

