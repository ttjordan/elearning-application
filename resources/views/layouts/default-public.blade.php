<html>
</head>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
    <meta charset='utf-8'>
    <title>Elearning App</title>

    <meta name='description' content='Elearning App'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0'>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/assets/vendors/font-awesome/css/font-awesome.min.css">
        <!-- NProgress -->
        <link rel="stylesheet" href="/assets/vendors/nprogress/nprogress.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/assets/vendors/iCheck/skins/flat/green.css">

        <!-- bootstrap-progressbar -->
        <link rel="stylesheet" href="/assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="/assets/vendors/jqvmap/dist/jqvmap.min.css">

        <!-- bootstrap-daterangepicker -->
        <link rel="stylesheet" href="/assets/vendors/bootstrap-daterangepicker/daterangepicker.css">

        <link rel="stylesheet" href="/assets/css/jqwidgets/styles/jqx.base.css">

        <!-- Bootstrap Toggle -->
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

        <link href="assets/css/main.css" rel="stylesheet">
        <link href="assets/css/publicpages.css" rel="stylesheet">

        @yield('styles')

</head>

<body>

    @include('includes.header-public')

	<!-- PAGE CONTENT BEGINS -->
		@yield('content')
	<!-- PAGE CONTENT ENDS -->

    <div id="footer">
        Copyright Your Site
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	@yield('scripts')
	

</body>
</html>