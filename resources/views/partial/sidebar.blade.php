<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">

    <ul class="nav side-menu">

      @if(isset($modules_menu))

      @foreach( $modules_menu->modules  as $module_menu)
      <li class="module active">

        @if(count($module_menu->slides) != 0)
          <a><i class="fa fa-book"></i> {{ $module_menu->menu_name}}<span class="fa fa-chevron-down"></span></a>

          <ul class="nav child_menu" style="display: block;">
            @foreach( $module_menu->slides as $slide_menu)
            <li id="slide-{{ $slide_menu->id}}" class="{ Request::segment(1) == 'current-staffing' ? 'current-page' : ''}}">
              <a href="/slides/{{ $slide_menu->id }}">
                {{ $slide_menu->menu_name }}
              </a>
            </li>
            @endforeach
          </ul>
    
        @else
        <a><i class="fa fa-book"></i> {{ $module_menu->menu_name}}<span class="fa fa-chevron-down"></span></a>
        @endif

      </li>
      @endforeach

      @endif

      @if(Auth::check())
      <li class="visible-xs visible-sm">
        <a href="/users/dashboard"><i class="menu-icon fa fa-caret-right"></i>
        Course Dashboard
        </a>
      </li>  			
      <li class="visible-xs visible-sm">
        <a href="/courses"><i class="menu-icon fa fa-caret-right"></i>
        All Courses
        </a>
      </li> 			
      <li class="visible-xs visible-sm">
        <a href="/modules"><i class="menu-icon fa fa-caret-right"></i>
        All Modules
        </a>
      </li>  			
      <li class="visible-xs visible-sm">
        <a href="/slides"><i class="menu-icon fa fa-caret-right"></i>
        All Slides
        </a>
      </li>    			
      <li class="visible-xs visible-sm">
        <a href="/users/approve-user"><i class="menu-icon fa fa-caret-right"></i>
        All Users
        </a>
      </li>  
      @endif
          
    </ul><!-- /.nav-list -->


<!-- /section:basics/sidebar.layout.minimize -->
  </div>
</div>
<!-- /sidebar menu -->



@section('scripts-sidebar')
<script>

	$('#slide-{{ Request::segment(2)}}').parents('.module').addClass('active open');
	$('#slide-{{ Request::segment(2)}}').addClass('active');
	
</script>
@stop
