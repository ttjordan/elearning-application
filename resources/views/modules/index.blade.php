@extends('layouts.default')
@section('content')

<div class="pull-right">
<a class="btn btn-primary" href="{{ URL::to('modules/create') }}">Create new Module</a>
<a class="btn btn-primary" href="{{ URL::to('slides/create') }}">Create new Slide</a>
</div>

<h1>All the Modules</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Menu Name</td>
			<td>Belongs To Course:</td>
			<td class="col-md-6">Actions</td>
		</tr>
	</thead>
	<tbody>
	@foreach($modules as $key => $value)
		<tr>
			<td>{{ $value->id }}</td>
			<td>{{ $value->name }}</td>
			<td>{{ $value->menu_name }}</td>
			<td>{{ isset($value->course_id) ? DB::table('courses')->where('id', $value->course_id)->pluck('name') : link_to('modules/'. $value->id.'/edit' , 'No assign course')  }}</td>
			<!-- we will also add show, edit, and delete buttons -->
			<td class="center">

				<!-- show the module (uses the show method found at GET /modules/{id} -->
				<a class="btn btn-small btn-success" href="{{ URL::to('modules/' . $value->id) }}">Show this Module</a>

				<!-- edit this module (uses the edit method found at GET /modules/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('modules/' . $value->id . '/edit') }}">Edit this Module</a>
				
				<!-- delete the module (uses the destroy method DESTROY /modules/{id} -->
				<!-- we will add this later since its a little more complicated than the first two buttons -->
				{{ Form::open(array('url' => 'modules/' . $value->id, 'style' => 'display: inline-block')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete this Module', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
				
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


@stop 