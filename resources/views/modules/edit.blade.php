@extends('layouts.default')

@section('styles')
<link rel="stylesheet" href="assets/css/jquery-cropbox-master/jquery.cropbox.css">
@stop

@section('content')

<a class="btn btn-primary pull-right" href="{{ URL::to('modules/create') }}">Create new Module</a>

<h1 class="header smaller lighter blue">Edit module {{ $module->name }}</h1>

<!-- if there are creation errors, they will show here -->
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif


{{ Form::model($module, array('action' => array('ModulesController@update', $module->id), 'files'=>true, 'method' => 'PUT')) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('name', 'Module Name:') }}
				{{ Form::text('name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('men_name', 'Menu Name:') }}
				{{ Form::text('menu_name', null, array('class' => 'form-control')) }}
			</div>
		</div>
		<!--
		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('description', 'Description:') }}
				{{ Form::textarea('description', old('description'), array('class' => 'form-control')) }}
			</div>
		</div>
		-->

		<div class="col-xs-12 col-md-4">
			<div class="form-group">
				{{ Form::label('course_id', 'Select Course') }}
				{{ Form::select('course_id',  $course_list, old('course_id'), array('class' => 'form-control')) }}

			</div>
		</div>


		<!--
		<div class="form-group">
			<div class="control-group">
				<div class="controls">
				{{ Form::label('image', 'Upload image') }}
				{{ Form::file('image', array('class'=>'upload-image')) }}
				  <p class="errors">{{ $errors->first('image')}}</p>
					@if(Session::has('error'))
					<p class="errors">{{ Session::get('error') }}</p>
					@endif
				</div>
			</div>
			<div id="success"> </div>
		</div>
		-->
	</div>
	{{ Form::submit('Save module changes!', array('class' => 'btn btn-primary', 'onchange' => 'imageInput()')) }}

{{ Form::close() }}

@stop

