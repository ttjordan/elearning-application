@extends('layouts.default')
			
@section('content')

<div class="page-header">
<h1>Course :: {{ $modules->name }} </h1>
</div>
<!-- if there are creation errors, they will show here -->
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif


<a href="{{$modules->id}}/edit">Edit module</a>


@stop 