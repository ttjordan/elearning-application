<?php

use App\Course as Course;
?>

@extends('layouts.default')
@section('content')

<a class="btn btn-primary pull-right" href="{{ URL::to('modules/create') }}">Create new Module</a>

<h1>Create a Module</h1>

<!-- if there are creation errors, they will show here -->
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

{{ Form::open(array('url' => 'modules', 'files'=>true )) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('name', 'Module Name:') }}
				{{ Form::text('name', old('name'), array('class' => 'form-control')) }}
			</div>
		</div>
		
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('menu_name', 'Menu Name:') }}
				{{ Form::text('menu_name', old('menu_name'), array('class' => 'form-control')) }}
			</div>
		</div>
			<!--
		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('description', 'Description') }}
				{{ Form::textarea('description', old('description'), array('class' => 'form-control')) }}
			</div>
		</div>
		
		<div class="form-group">
			<div class="control-group">
				<div class="controls">
				{{ Form::label('image', 'Upload image') }}
				{{ Form::file('image') }}
				  <p class="errors">{{ $errors->first('image')}}</p>
					@if(Session::has('error'))
					<p class="errors">{{ Session::get('error') }}</p>
					@endif
				</div>
			</div>
			<div id="success"> </div>
		</div>
		-->
	</div>
	{{ Form::submit('Create the Module!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

 


@stop 