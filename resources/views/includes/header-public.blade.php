<!-- Landing Navbar (need to remove app navbar) -->
@if (Request::path() == '/')
	<nav class="navbar navbar-default" role="navigation">
@elseif (Request::path() == 'page/materials')
	<nav class="navbar navbar-default navbar-materials" role="navigation">
@elseif (Request::path() == 'page/videos')
	<nav class="navbar navbar-default navbar-videos" role="navigation">
@else
	<nav class="navbar navbar-default navbar-showcase" role="navigation">
@endif

  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	  <span class="sr-only">Toggle navigation</span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	</button>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">

	<ul class="nav navbar-nav pull-right">

		@if(!Auth::check())
			<!-- if logged in, won't see Register link -->
			<li><a href="/users/register">Register</a></li>
			<li><a href="/login">Login</a></li>
		@else
			<li {{ Request::path() == '/users' ? 'class="active"' : '' }}><a href="/users/{{ Auth::user()->id}}">You're logged in <i class="menu-icon fa fa-sign-in"> </i></a>
			</li>
		@endif
	</ul>
  </div><!-- /.navbar-collapse -->



</nav>
