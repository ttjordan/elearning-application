<ul class="breadcrumb">

    <li>
        <a href="/">
            <i class="ace-icon fa fa-home home-icon"></i>Home
        </a>
    </li>

    @foreach($breadcrumbs as $breadcrumb)

        @if($breadcrumb == end($breadcrumbs))
        <li class="active">
            {{ $breadcrumb['name'] }}
        </li>
        @else
        <li>
            <a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['name'] }}</a>
        </li>
        @endif

    @endforeach

</ul><!-- .breadcrumb -->