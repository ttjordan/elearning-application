
<!-- #section:basics/sidebar -->

<div id="sidebar" class="sidebar navbar-nav {{ Auth::check() ? '' : 'menu-min'}} responsive">
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	</script>


	<ul class="nav nav-list" style="top: 0px;">

		@if(isset($modules_menu))

			@foreach( $modules_menu->modules  as $module_menu)
			<li class="module" class="">
			
			@if(count($module_menu->slides) != 0)
			
				<a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-book"></i>
					<span class="menu-text">
						{{ $module_menu->menu_name}}
					</span>

					<b class="arrow fa fa-angle-down"></b>
				</a>
		
				<ul class="submenu">
					@foreach( $module_menu->slides as $slide_menu)
					<li id="slide-{{ $slide_menu->id}}" class="">
						<a href="/slides/{{ $slide_menu->id }}">
							<i class="menu-icon fa fa-caret-right"></i>
							{{ $slide_menu->menu_name }}
						</a>

						<b class="arrow"></b>
					</li>
					@endforeach
				</ul>
			@else
				<a href="#">
					<i class="menu-icon fa fa-book"></i>
					<span class="menu-text">
						{{ $module_menu->menu_name}}
					</span>

				</a>
			@endif
			
			</li>
			@endforeach
			
		@endif
			
		@if(Auth::check())
			<li class="visible-xs visible-sm">
				<a href="/users/dashboard"><i class="menu-icon fa fa-caret-right"></i>
				Course Dashboard
				</a>
			</li>  			
			<li class="visible-xs visible-sm">
				<a href="/courses"><i class="menu-icon fa fa-caret-right"></i>
				All Courses
				</a>
			</li> 			
			<li class="visible-xs visible-sm">
				<a href="/modules"><i class="menu-icon fa fa-caret-right"></i>
				All Modules
				</a>
			</li>  			
			<li class="visible-xs visible-sm">
				<a href="/slides"><i class="menu-icon fa fa-caret-right"></i>
				All Slides
				</a>
			</li>    			
			<li class="visible-xs visible-sm">
				<a href="/users/approve-user"><i class="menu-icon fa fa-caret-right"></i>
				All Users
				</a>
			</li>  
		@endif
				
		</ul><!-- /.nav-list -->


	<!-- /section:basics/sidebar.layout.minimize -->
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>


@section('scripts-sidebar')
<script>

	$('#slide-{{ Request::segment(2)}}').parents('.module').addClass('active open');
	$('#slide-{{ Request::segment(2)}}').addClass('active');
	
</script>
@stop
