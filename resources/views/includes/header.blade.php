<?php

use App\User as User;

// get user information
$user = User::where('id','=', Auth::id())->first(); 
?>

<!-- Landing Navbar (need to remove app navbar) -->
<nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->

		<div class="pull-left visible-lg">
			<a href="/" class="navbar-brand">
				<!--
				<img src="/assets/images/logo-no-tag.png" height="40px">

				-->

				|| <b>Project Lift</b> ||
			</a>
		</div>

		<div class="pull-left visible-md visible-lg">
			 <div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
				@if(Auth::check())
					<li >
						<a {{ Request::path() == '/users/dashboard' ? 'class="active"' : '' }} href="/users/dashboard">
							<i class="fa fa-book" aria-hidden="true"></i> Course Dashboard
						</a>
					</li>  
				@endif
				</ul>
			</div>
		</div>
		
		@if(isset($user->firstname))
		<div class="pull-right visible-md visible-lg">
			<ul class="nav navbar-nav">
			<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
				
						<span class="user-info">
							<small>Hi,</small>
							
							{{ $user->firstname }}
						</span>

						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li>
							<a href="/users/{{$user->id}}/edit">
								<i class="ace-icon fa fa-cog"></i>
								Settings
							</a>
						</li>

						<li>
						
							<a href="/users/{{ $user->id }}">
								<i class="ace-icon fa fa-user"></i>
								users
							</a>
						</li>

						<li class="divider"></li>
						
						<li>
							<a href="{{ route('logout') }}"
								onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
								<i class="ace-icon fa fa-power-off"></i>
								Logout
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
                        </li>

					</ul>
				</li>
			</ul>
		</div>

		<div class="pull-right visible-md visible-lg">
			 <div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
				@if(Auth::check())
				<!-- TODO FIX HASROLE  ADD START(AT SYMBOL)  if( Auth::user()->hasRole("superadmin"))-->
					<li {{ Request::path() == 'courses' ? 'class="active"' : '' }}><a href="{{url('/courses')}}">Courses</a></li>  
					<li {{ Request::path() == 'modules' ? 'class="active"' : '' }}><a href="{{url('/modules')}}">Modules</a></li>  
					<li {{ Request::path() == 'slides' ? 'class="active"' : '' }}><a href="{{url('/slides')}}">Slides</a></li>  
					<li {{ Request::path() == 'users' ? 'class="active"' : '' }}><a href="{{url('/users')}}">Users</a></li>  
			<!-- ADD CLOSE(AT SYMBOL) endif-->
				@endif
				</ul>
			</div>
		</div>
		@endif

</nav>


		