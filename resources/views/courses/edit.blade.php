@extends('layouts.default')
@section('content')

<a class="btn btn-primary pull-right" href="{{ URL::to('courses/create') }}">Create new Course</a>

<h1 class="header smaller lighter blue">Edit module {{ $course->name }}</h1>

<!-- if there are creation errors, they will show here -->
@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif


{{ Form::model($course, array('action' => array('CoursesController@update', $course->id), 'method' => 'PUT')) }}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('name', 'Course Name:') }}
				{{ Form::text('name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('men_name', 'Menu Name:') }}
				{{ Form::text('menu_name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				{{ Form::label('hours', 'Hours, select one:') }}<br>
				{{ Form::select('hours', array(0,1,2,3,4,5,6), old('hours')) }}<br>
				<span class="help-block small">Indicate how long this course should take to complete.</span>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group">
				{{ Form::label('description', 'Description:') }}
				{{ Form::textarea('description', null, array('class' => 'form-control')) }}
				<span class="help-block small">Provide a short description on this course.</span>
			</div>
		</div>
	</div>

	{{ Form::submit('Save course changes!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop