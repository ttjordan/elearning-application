<?php

use App\Course as Course;
use App\Module as Module;
use App\Enrollment as Enrollment;
?>

@extends('layouts.default')
@section('content')

<?php

	// TODO MOVE TO CONTROLLER...
	$allSlides = Course::find($course->id);

	// create query attributes
	$matchThese = [ 'module_id' => $course->id ,'enrollment_status_id' => 3, 'user_id' => Auth::id()];
	// get slides that where completed
	$slidesCompleted = Enrollment::where($matchThese)->count();
	
	
	// get remaining slide
	if($allSlides->slides->count() != 0){
		$slidesRemaining = $allSlides->slides->count() - $slidesCompleted;
	}else{
		$slidesRemaining = 0;
	}


	$setPerNum = 0;

	//var_dump( $allSlides->slides->count() );
	//var_dump( $slidesCompleted );

	//dd( $allSlides->slides->count() - $slidesCompleted );

	if($allSlides->slides->count() != 0){
		// divide completed by slide count
		$percent =  $slidesCompleted ? $slidesCompleted / $allSlides->slides->count() : $setPerNum; // set 0 if no modules or slide exist.
	}else{
		$percent = $setPerNum;
	}
	
	// the percent will have  decimal, only need part of that
	$percentformat = number_format((float)$percent, 2, '', '');
	// need to return part of the string
	$percentStr = substr($percentformat, 1);

	// if all the slides are done then, change the variable 100%
	if($allSlides->slides->count() == $slidesCompleted){
		$percentStr = 100;
	}
?>

<!-- TODO FIX HASROLE  ADD START(AT SYMBOL)  if( Auth::user()->hasRole("superadmin"))-->

<div class="row">
	<div class="col-md-12">
		<div class="pull-right" style="margin-left: 5px;">
			<a class="btn btn-primary" href="{{$course->id}}/edit">Edit course</a>
		</div>
	</div>
</div>
<!-- ADD CLOSE(AT SYMBOL) endif-->

<h1 class="header smaller lighter blue">


	<span class="col-md-10"><i class="glyphicon glyphicon-education" aria-hidden="true"></i> {{ $course->name }} </span>


	<div class="slide-stats col-md-2">
		<p class="pull-right">
		{{ 'Total Slides: ' . $allSlides->slides->count() }} <br>
		{{ 'Slides Remaining:'. $slidesRemaining }} <br>
		{{ 'Completed:'. $slidesCompleted }} <br>
		</p>
	</div>



<div class="clearfix"></div>
</h1>



<div class="row">
	<div class="col-md-12 col-centered">

		@if(!empty($course->description))
		<p class="lead"> {{ $course->description }} </p>
		<hr>
		@endif

		@foreach($modules as $module)
		
			<h3>{{ $module->name}}</h3>

			<div class="row">
				@foreach($module->slides as $slide)

					<div class="col-md-3 col-sm-4 col-xs-6 panel panel-default">
					
						@if($imageObject->filename  != NULL)
						<div class="course-card-image">
							<img src="/assets/uploads/{{ $imageObject['filename'] }} " alt="">
							<!-- <div class="completion-medal flaticon-medal2 "></div> -->
						</div>
						@else
						<div class="course-card-image">
							<img src="http://placehold.it/400x200" alt="">
						</div>
						@endif
						<div class="panel-body">
							

							<?php 
							$matchThese = ['slide_id' => $slide->id, 'user_id' => Auth::id()];
							$enrollment = Enrollment::where($matchThese)->first(); 
							?>

							@if($enrollment['user_id'] == Auth::id())
							
								<?php
								  switch($enrollment['enrollment_status_id']) 
								  {
									case 1:
										echo $slide->name . ': Started <br>';
										break;
									case 2:
										echo '<a href="/slides/'.$slide->id.'">'. $slide->name . '</a>: Continue<br>';
										break;
									case 3:	
										echo '<a href="/slides/'.$slide->id.'">'. $slide->name . '</a><div class="completion-medal flaticon-medal2 "></div>';
										 echo ': <i class="fa fa-check green"></i> Completed<br>';
										break;	
								  }
								?>
							
							@if($slide->description)
								<div class="course-card-info">
								{{ $slide->description }}...
								</div>
							@endif

							@else
								<?php  echo '<a href="/slides/'.$slide->id.'">'. $slide->name . '</a>';?>
								
								@if($slide->description)
								<div class="course-card-info">
								{{ $slide->description }}...
								</div>
								@endif
							@endif
							<div class="hr hr12 hr-dotted"></div>
							<div class="pull-right">
								<a href="{{ URL::to('/slides', $slide->id) }}" ><button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-play" aria-hidden="true"></span> Play </button></a>
							</div>
						</div>
					</div>
			  	@endforeach
			</div>

			<div class="hr hr32 hr-dotted"></div>

		@endforeach

	
		</div>
	</div>
@stop 