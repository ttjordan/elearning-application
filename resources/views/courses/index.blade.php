@extends('layouts.default')
@section('content')


<a class="btn btn-primary pull-right" href="{{ URL::to('courses/create') }}">Create new Course</a>


<h1 class="header smaller lighter blue">All the Courses</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Menu Name</td>
			<td class="col-md-5">Actions</td>
		</tr>
	</thead>
	<tbody>
	@foreach($courses as $key => $value)
		<tr>
			<td>{{ $value->id }}</td>
			<td>{{ $value->name }}</td>
			<td>{{ $value->menu_name }}</td>

			<!-- we will also add show, edit, and delete buttons -->
			<td class="center">
			
				<!-- show the course (uses the show method found at GET /courses/{id} -->
				<a class="btn btn-small btn-success" href="{{ URL::to('courses/' . $value->id) }}">Show Course + Slides</a>

				<!-- edit this course (uses the edit method found at GET /courses/{id}/edit -->
				<a class="btn btn-small btn-info" href="{{ URL::to('courses/' . $value->id . '/edit') }}">Edit Course</a>

				<!-- delete the course (uses the destroy method DESTROY /courses/{id} -->
				<!-- we will add this later since its a little more complicated than the first two buttons -->
				{{ Form::open(array('url' => 'courses/' . $value->id, 'style' => 'display: inline-block')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Delete Course', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
				
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


@stop 