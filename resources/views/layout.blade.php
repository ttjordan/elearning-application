<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">



        <title> Lahey Provider on Boarding </title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="/assets/vendors/font-awesome/css/font-awesome.min.css">
        <!-- NProgress -->
        <link rel="stylesheet" href="/assets/vendors/nprogress/nprogress.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="/assets/vendors/iCheck/skins/flat/green.css">

        <!-- bootstrap-progressbar -->
        <link rel="stylesheet" href="/assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="/assets/vendors/jqvmap/dist/jqvmap.min.css">

        <!-- bootstrap-daterangepicker -->
        <link rel="stylesheet" href="/assets/vendors/bootstrap-daterangepicker/daterangepicker.css">

        <link rel="stylesheet" href="/assets/css/jqwidgets/styles/jqx.base.css">

        <!-- Bootstrap Toggle -->
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link rel="stylesheet" href="/assets/css/custom.css">

       @yield('styles')

</head>

    <body class="nav-md">
        <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                
                <div class="navbar nav_title" style="border: 0;">
                
                <a href="/" class="site_title">eLearning Course</a>
                </div>

                <div class="clearfix"></div>

                    @include('partial.quick_menu')

                    @include('partial.sidebar')

                    @include('partial.menu_footer')           

                </div>

            </div>

            @include('partial.top_navigation')

            <!-- page content -->
            @yield('content')
            <!-- /page content -->

            @include('partial.footer')


        </div>
        </div>
  

        <!-- jQuery -->
        <script src="/assets/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="/assets/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="/assets/vendors/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="/assets/vendors/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="/assets/vendors/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="/assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="/assets/vendors/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="/assets/vendors/skycons/skycons.js"></script>
        <!-- Flot -->
        <script src="/assets/vendors/skycons/skycons.js"></script>
        <script src="/assets/vendors/Flot/jquery.flot.js"></script>
        <script src="/assets/vendors/Flot/jquery.flot.pie.js"></script>
        <script src="/assets/vendors/Flot/jquery.flot.time.js"></script>
        <script src="/assets/vendors/Flot/jquery.flot.stack.js"></script>
        <script src="/assets/vendors/Flot/jquery.flot.resize.js"></script>

        <!-- Flot plugins -->
        <script src="/assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="/assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="/assets/vendors/flot.curvedlines/curvedLines.js"></script>

        <!-- DateJS -->
        <script src="/assets/vendors/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="/assets/vendors/jqvmap/dist/jquery.vmap.js"></script>
        <script src="/assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="/assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="/assets/vendors/moment/min/moment.min.js"></script>
        <script src="/assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- jquery.steps-1.1.0 -->
        <script src="/assets/vendors/jquery.steps-1.1.0/jquery.steps.min.js"></script>

        <!-- jquery.steps-1.1.0 -->
        <script src="/assets/vendors/validator/validator.js"></script>

        <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <script src="https://cdn.datatables.net/rowgroup/1.0.2/js/dataTables.rowGroup.min.js"></script>

        <script src="https://rawgit.com/wenzhixin/bootstrap-table/master/src/bootstrap-table.js"></script>
       
        <script src="/assets/vendors/jqwidgets/jqx-all.js"></script>

        <!-- Bootstrap Toggle  -->
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="/assets/js/custom.js"></script>
        @yield('scripts')


    </body>
</html>
