@extends('layouts.default')
@section('content')

<h1 class="header smaller lighter blue">User Status</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="row">
	<div class="col-xs-12">
	
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>


		<!-- div.table-responsive -->

		<!-- div.dataTables_borderWrap -->
		<div>
			<table id="dynamic-table" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th class="text-center">
							<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>
						</th>
						<th>User Name</th>
						<th>Email</th>
						<th class="text-center">Status</th>
						<th class="col-md-2 text-right">Actions</th>
					</tr>
				</thead>

				<tbody>
				
				@foreach($users as $user)
					<tr>
						<td class="text-center">
							<label class="pos-rel">
								<input type="checkbox" class="ace" />
								<span class="lbl"></span>
							</label>
						</td>

						<td>
							<a href="/users/{{$user->id}}" >{{ $user->firstname}}	 {{ $user->lastname}}</a>
						</td>
						<td>{{ $user->email}}</td>
						<td class="text-center">
							{!! $user->approval == 1  ? '<span class="label label-sm label-success">approved</span>' : '<span class="label label-sm label-warning">pending</span>' !!}
						</td>

						<td class="text-right">
							<div class="hidden-sm hidden-xs action-buttons">
								<a class="blue" href="/users/{{$user->id}}">
									<i class="ace-icon fa fa-search-plus bigger-130"></i>
								</a>

								<a class="green" href="/users/{{$user->id}}/edit" >
									<i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>
								
								<!-- delete the users (uses the destroy method DESTROY /users/{id} -->
								<!-- we will add this later since its a little more complicated than the first two buttons -->
								{{ Form::open(array('url' => 'users/' . $user->id, 'style' => 'display: inline-block')) }}
									{{ Form::hidden('_method', 'DELETE') }}
									<a class="red" href="#" onclick="$(this).closest('form').submit(); return false;"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
								{{ Form::close() }}
				
								<a class="red" href="#">
									
								</a>
							</div>

							<div class="hidden-md hidden-lg">
								<div class="inline pos-rel">
									<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
										<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
									</button>

									<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
										<li>
											<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
												<span class="blue">
													<i class="ace-icon fa fa-search-plus bigger-120"></i>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
												<span class="green">
													<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
												</span>
											</a>
										</li>

										<li>
											<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
												<span class="red">
													<i class="ace-icon fa fa-trash-o bigger-120"></i>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</td>
					</tr>

				@endforeach
				
		
				</tbody>
			</table>
		</div>
	</div>
</div>


@stop


@section('scripts')


@stop