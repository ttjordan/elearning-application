@extends('layouts.default')
@section('content')

<h1 class="header smaller lighter blue">Courses</h1>

<div class="row">
	<div class="col-md-12">

	@foreach($courses as $course)
		<div class="col-sm-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading text-right"> 
						{{ $course->hours }} hours
				</div>
	 			 <div class="panel-body">
					<h3> <a href="{{ route('courses.show', $course->id) }}"> <span class="glyphicon glyphicon-education" aria-hidden="true"></span> {{ $course->name }}</a></h3>

					<div class="hr hr-single hr-dotted hr18"></div>

					<a href="{{ route('courses.show', $course->id) }}" class="btn btn-xs btn-primary" >Explore</a>
					<a href="#" class="btn btn-xs btn-primary" data-toggle="popover" data-trigger="hover" data-content="{{ $course->description }}">Description</a>
					
					@if(Auth::user()->hasRole("superadmin"))
					<a href="{{{ route('courses.edit', $course->id) }}}" class="pull-right" >Edit</a>
					@endif
				</div>
			</div>
		</div>
	@endforeach

	</div>
</div>


@stop

@section('scripts')
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>

@stop 