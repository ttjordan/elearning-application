@extends('layouts.default')
@section('content')

<div class="page-header">
	<h1>User Profile
	
	<div class="pull-right">


		@if(Auth::user()->authorizeRoles(['superadmin', 'admin']))
		<a href="/users/{{ Auth::user()->id }}/edit " class="btn btn-xs btn-primary">
		<i class="ace-icon fa fa-pencil-square-o"></i> Edit Profile</a>
		@endif

	</div></h1>
</div>

@if(Session::has('message'))
	<h4 class="text-success">{{ Session::get('message') }}</h4>
@endif

<div class="profile-user-info profile-user-info-striped">
	<div class="profile-info-row">
		<div class="profile-info-name"> First name</div>

		<div class="profile-info-value">
			<span class="editable" id="username">{{ Auth::user()->firstname }}</span>
		</div>
	</div>
	
	<div class="profile-info-row">
		<div class="profile-info-name">Last name</div>

			
		<div class="profile-info-value">
			<span class="editable" id="username">{{ Auth::user()->lastname }}</span>
		</div>
	</div>
	
	<div class="profile-info-row">
		<div class="profile-info-name">Email</div>

			
		<div class="profile-info-value">
			<span class="editable" id="username">{{ Auth::user()->email }}</span>
		</div>
	</div>	

</div>



@stop