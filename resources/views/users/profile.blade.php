@extends('layouts.default')
@section('content')


<div class="page-header">
<h1>users</h1>
</div>

{{ Form::model($user, array('route' => array('users.update', $user->id)), 'POST') }}

<input type="hidden" name="_method" value="PUT" />
	<ul>
		<li>
			{{ Form::label('firstname', 'Firstname:') }}
			{{ Form::text('firstname') }}
		</li>
		<li>
			{{ Form::label('lastname', 'Lastname:') }}
			{{ Form::text('lastname') }}
		</li>
		<li>
			{{ Form::label('role_id', 'Role_id:') }}
			{{ Form::text('role_id') }}
		</li>
		<li>
			{{ Form::label('password', 'Password:') }}
			{{ Form::text('password') }}
		</li>
		<li>
			{{ Form::label('email', 'Email:') }}
			{{ Form::text('email') }}
		</li>
		<li>
			{{ Form::label('remember_token', 'Remember_token:') }}
			{{ Form::text('remember_token') }}
		</li>
		<li>
			{{ Form::label('created_at', 'Created_at:') }}
			{{ Form::text('created_at') }}
		</li>
		<li>
			{{ Form::label('updated_at', 'Updated_at:') }}
			{{ Form::text('updated_at') }}
		</li>
		<li>
			{{ Form::submit() }}
		</li>
	</ul>
{{ Form::close() }}

@stop