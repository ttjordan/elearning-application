@extends('layouts.default')
@section('content')


<div class="page-header">
<h1>User profile</h1>
</div>

{{ Form::model($user, array('route' => array('users.update', $user->id)), 'POST') }}

@if(Session::has('message'))
	<p class="alert">{{ Session::get('message') }}</p>
@endif

<input type="hidden" name="_method" value="PUT" />

<div class="profile-user-info profile-user-info-striped">
	<div class="profile-info-row">
		<div class="profile-info-name"> {{ Form::label('firstname', 'First name') }} </div>

		<div class="profile-info-value">
			<span class="editable editable-click" id="username"> {{ Form::text('firstname') }}</span>
		</div>
	</div>
	
	<div class="profile-info-row">
		<div class="profile-info-name"> {{ Form::label('lastname', 'Last name') }}</div>

		<div class="profile-info-value">
			<span class="editable editable-click" id="username"> {{ Form::text('lastname') }}</span>
		</div>
	</div>
	
	<div class="profile-info-row">
		<div class="profile-info-name"> {{ Form::label('email', 'Email') }}</div>

		<div class="profile-info-value">
			<span class="editable editable-click" id="username"> {{ Form::text('email') }}</span>
		</div>
	</div>		
	
	<div class="profile-info-row">
		<div class="profile-info-name">Password</div>

		<div class="profile-info-value">
			<a href="/change-password" class="editable editable-click" id="username">Chanage password</a>
		</div>
	</div>	
	
	
	@if($userRole->hasRole("superadmin"))
	<div class="profile-info-row">
		<div class="profile-info-name">{{ Form::label('email', 'Approve User') }}</div>
		<div class="profile-info-value">
			<span class="editable" id="username">{{ Form::checkbox('approval', 1, null, ['id'=>'id-button-borders', 'class' => 'ace ace-switch ace-switch-5']) }}
			<span class="lbl middle"></span>
			</span>
		</div>
	</div>
	@endif
</div>




<br>
<br>
{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop