<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
		
		$roles  = [
				['id' => 1, 'name' => 'superadmin', 'slug' => 'superadmin', 'rank' => 1],
				['id' => 2, 'name' => 'admin', 'slug' => 'admin', 'rank' => 2],
				['id' => 3, 'name' => 'student', 'slug' => 'student', 'rank' => 3]
				];
				
		DB::table('roles')->insert($roles);
    }
}