<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        $user = new App\User;

        $user->fill(array(
        	'email'	   => 'superadmin@test.com',
        	'name' => 'timjordan',
        	'role_id' => '1',
        	'firstname'	   => 'tim',
        	'lastname'	   => 'jordan',
        	'approval'	   => 1
        	));

       	$user->password = Hash::make('password');

       	$user->save();
    }

}