<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();
		
		$permissions  = [
				['id' => 1, 'name' => 'manage_users', 'display_name' => 'manage_users']
				];	
				
		$permission_role  = [
				['id' => 1, 'permission_id' => 1, 'role_id' => 1]
				];
				
		DB::table('permissions')->insert($permissions);
		DB::table('permission_role')->insert($permission_role);
    }
}