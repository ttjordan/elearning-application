<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
		Eloquent::unguard();
		$this->call('UserTableSeeder');
		$this->call('RoleTableSeeder');
		$this->call('RoleUserTableSeeder');
		$this->call('PermissionsTableSeeder');
		//$this->call('CourseTableSeeder');
		//$this->call('CourseStatusSeeder');
		$this->call('EnrollmentStatusSeeder');
		$this->command->info('Done! All tables seeded!');
    }
}
