<?php

use Illuminate\Database\Seeder;

class EnrollmentStatusSeeder extends Seeder {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enrollment_status')->delete();
		
		$enrollment = [
				['id' => 1, 'name' => 'not_started', 'slug' => 'not_started', 'rank' => 1],
				['id' => 2, 'name' => 'continue', 'slug' => 'continue', 'rank' => 2],
				['id' => 3, 'name' => 'completed', 'slug' => 'completed', 'rank' => 3]
				];
				
		DB::table('enrollment_status')->insert($enrollment);
    }
}