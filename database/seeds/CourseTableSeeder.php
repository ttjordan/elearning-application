<?php

use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->delete();
        DB::table('modules')->delete();
        DB::table('slides')->delete();
		
		$courses  = [
				['id' => 1, 'name' => 'The Affordable Care Act', 'menu_name' => ''] ,
				['id' => 2, 'name' => 'Another Course Two', 'menu_name' => ''] 
				];
				
		$modules  = [
				['id' => 1, 'name' => 'The Basics', 'menu_name' => 'The Basics', 'course_id' => 1],
				['id' => 2, 'name' => 'Drivers', 'menu_name' => 'Drivers', 'course_id' => 1],
				['id' => 3, 'name' => 'Another module 2', 'menu_name' => 'another name', 'course_id' => 2]
				];
				
		$slides  = [
				['id' => 1, 'name' => 'Purpose', 'menu_name' => 'Purpose', 'module_id' => 1],
				['id' => 2, 'name' => 'Learning Objectives', 'menu_name' => 'Learning Objectives', 'module_id' => 1],
				['id' => 3, 'name' => 'Rising healthcare costs', 'menu_name' => 'Rising healthcare costs', 'module_id' => 2],
				['id' => 4, 'name' => 'another name', 'menu_name' => 'some name', 'module_id' => 3]
				];
				
		DB::table('courses')->insert($courses);
		DB::table('modules')->insert($modules);
		DB::table('slides')->insert($slides);
    }
}