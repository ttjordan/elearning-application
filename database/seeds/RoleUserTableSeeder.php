<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('role_user')->delete();
		
		$enrollment = [
				['id' => 1, 'user_id' => 1, 'role_id' => 1]
				];
				
		DB::table('role_user')->insert($enrollment);
    }
	
}