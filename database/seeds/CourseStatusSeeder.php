<?php

use Illuminate\Database\Seeder;

class CourseStatusSeeder extends Seeder {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses_status')->delete();
		
		$enrollment = [
				['id' => 1, 'course_id' => 1, 'user_id' => 1, 'status' => 1],
				['id' => 3, 'course_id' => 2, 'user_id' => 1, 'status' => 0]
				];
				
		DB::table('courses_status')->insert($enrollment);
    }
}