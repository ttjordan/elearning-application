<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration {

	public function up()
	{
 		Schema::table('courses_status', function(Blueprint $table) {
			$table->foreign('course_id')->references('id')->on('courses')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('courses_status', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->foreign('course_id')->references('id')->on('courses')
						->onDelete('cascade')
						->onUpdate('cascade');
		});		
		Schema::table('slides', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->foreign('enrollment_status_id')->references('id')->on('enrollment_status')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->foreign('slide_id')->references('id')->on('slides')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('fileentries', function(Blueprint $table) {
			$table->foreign('module_id')->references('id')->on('modules')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('fileentries', function(Blueprint $table) {
			$table->foreign('slide_id')->references('id')->on('slides')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('courses_status', function(Blueprint $table) {
			$table->dropForeign('courses_status_course_id_foreign');
		});
		Schema::table('courses_status', function(Blueprint $table) {
			$table->dropForeign('courses_status_user_id_foreign');
		});
		Schema::table('modules', function(Blueprint $table) {
			$table->dropForeign('slides_course_id_foreign');
		});		
 		Schema::table('slides', function(Blueprint $table) {
			$table->dropForeign('slides_module_id_foreign');
		});

		Schema::table('enrollment', function(Blueprint $table) {
			$table->dropForeign('enrollment_module_id_foreign');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->dropForeign('enrollment_user_id_foreign');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->dropForeign('enrollment_enrollment_status_id_foreign');
		});
		Schema::table('enrollment', function(Blueprint $table) {
			$table->dropForeign('enrollment_slide_id_foreign');
		});
		Schema::table('fileentries', function(Blueprint $table) {
			$table->dropForeign('fileentries_module_id_foreign');
		});		
		Schema::table('fileentries', function(Blueprint $table) {
			$table->dropForeign('fileentries_slide_id_foreign');
		});
	}
}