<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentStatusTable extends Migration {

	public function up()
	{
		Schema::create('enrollment_status', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 45)->nullable();
			$table->string('slug', 45);
			$table->string('rank', 45);
		});
	}

	public function down()
	{
		Schema::drop('enrollment_status');
	}
}