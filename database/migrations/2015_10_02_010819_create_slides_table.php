<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration {

	public function up()
	{
		Schema::create('slides', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('module_id')->unsigned()->nullable();
			$table->string('menu_name', 255)->nullable();
			$table->string('description', 255)->nullable();
			$table->string('name', 255)->nullable();
			$table->string('media_script', 255)->nullable();
			$table->text('question', 255)->nullable();
			$table->longText('answers')->nullable();
			$table->longText('transcript')->nullable();
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('slides');
	}
}