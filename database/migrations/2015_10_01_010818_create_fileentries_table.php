<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileentriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fileentries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('module_id')->unsigned()->nullable();
			$table->integer('slide_id')->unsigned()->nullable();
			$table->string('filename')->nullable();
			$table->string('mime')->nullable();
			$table->string('original_filename')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fileentries');
	}

}
