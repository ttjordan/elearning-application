<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollmentTable extends Migration {

	public function up()
	{
		Schema::create('enrollment', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('module_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('enrollment_status_id')->unsigned();
			$table->integer('slide_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('enrollment');
	}
}