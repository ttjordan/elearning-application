<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model {

	protected $table = 'enrollment';
	public $timestamps = false;

	public function enrollment_status()
	{
		return $this->hasOne('App\Enrollment_Status');
	}

	public function module()
	{
		return $this->belongsTo('App\Module', 'module_id');
	}	
	
	public function slide()
	{
		return $this->belongsTo('App\Slides', 'resume_slide_id');
	}	
	
}