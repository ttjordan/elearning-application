<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Course as Course;
use App\Module as Module;
use App\User as User;
use App\Fileentry as Fileentry;

use Session;

class CoursesController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
	// protected from unauthorized users, checks if the current user is logged in. If the user is not logged in, they get redirected to the login page
	/**
	public function __construct() {
		$this->beforeFilter('auth');
		$this->beforeFilter('has_role_superadmin', array('only' => array('index','create', 'store', 'edit', 'update', 'destroy')));
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
 	*/

	protected $layout = "layouts.default";
	
	public function index()
	{
		// get all the courses
		$courses = Course::all();

		// load the view and pass the courses
		return view('courses.index')->with('courses', $courses);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// load the create form (app/views/courses/create.blade.php)
		return view('courses.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = [
			'name'       => 'required',
			'menu_name'  => 'required',
			'hours'      => 'required|integer'
		];

        $this->validate($request, $rules);

		// store
		$course = new Course;
		$course->name       	= $request->get('name');
		$course->menu_name      = $request->get('menu_name');
		$course->hours      	= $request->get('hours');
		$course->description    = $request->get('description');
		$course->save();

        // save data
        if ($course->save()) {
			// redirect
			Session::flash('message', 'Successfully created course!');
			return Redirect('courses');
        } else {
            Session::flash('success', 'No record created.');
            return back()->withInput();

        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// show the view and pass the course to it
		$course = Course::find($id);
		$modules = $course->modules;
		// get user information
		$user = User::where('id','=', Auth::id())->first(); 
		//$modules = Module::with('enrollment','slides')->get();
		$imageObject = Fileentry::find($id);


		return view('courses.show', array(
			'course'=>$course, 
			'modules'=> $modules, 
			'user' => $user,
			'imageObject' => $imageObject
		));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// get the course
		$course = Course::find($id);
		
		// show the edit form and pass the course
		return view('courses.edit')->with('course', $course);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update(Request $request, $id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = [
			'name'       => 'required',
			'menu_name'  => 'required',
			'hours'      => 'required|integer'
		];

		$this->validate($request, $rules);
		
		// store
		$course = Course::find($id);
		$course->name        = $request->get('name');
		$course->menu_name	 = $request->get('menu_name');
		$course->hours       = $request->get('hours');
		$course->description = $request->get('description');
		$course->save();

        // save data
        if ($course->save()) {
			// redirect
			Session::flash('message', 'Successfully updated course!');
            return Redirect('courses');
        } else {
            Session::flash('success', 'No record created.');
            return back()->withInput();

        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$course = Course::find($id);
		$course->delete();

		// redirect
		Session::flash('message', 'Successfully deleted the course!');
		return Redirect('courses');
	}

}