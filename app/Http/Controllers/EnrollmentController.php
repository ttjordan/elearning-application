<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Module as Module;
use App\Enrollment as Enrollment;

use Session;

class EnrollmentController extends Controller {

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {

	$matchThese = ['module_id' => $request->get('module_id'), 'user_id' => $request->get('user_id'), 'enrollment_status_id' => 3, 'slide_id' => $request->get('slide_id')];
	$enrollmentInDB = Enrollment::where($matchThese)->exists();

	//dd($enrollment);
	
	if (!$enrollmentInDB){
		// store
		$enrollment = new Enrollment;
		$enrollment->module_id	= $request->get('module_id');
		$enrollment->user_id	= $request->get('user_id');
		$enrollment->enrollment_status_id	= $request->get('enrollment_status_id');
		$enrollment->slide_id	= $request->get('slide_id');
		
		$enrollment->save();

		$nextPage	= $request->get('nextPage');
		
		// redirect
		if(!empty($nextPage)){
			Session::flash('message', 'Start Slide!');
			return Redirect('slides/' . $nextPage);
		}else{
			$module = Module::find($enrollment->module_id);
			Session::flash('message', 'Module Completed!');
			return Redirect('courses/'. $module->course_id);
		}
	}else{
		$nextPage	= $request->get('nextPage');
		$enrollment = new Enrollment;
		$enrollment->module_id	= $request->get('module_id');
		// redirect
		if(!empty($nextPage)){
			Session::flash('message', 'Start Slide!');
			return Redirect('slides/' . $nextPage);
		}else{
			$module = Module::find($enrollment->module_id);
			Session::flash('message', 'Module '. $module->id .' Completed!');
			return Redirect('courses/'. $module->course_id);
		}
	}	
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
    
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }
  
}

?>