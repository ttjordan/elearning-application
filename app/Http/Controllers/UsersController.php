<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Course as Course;

class UsersController extends Controller{

    /**
     * Middleware to check for Unauthorized users.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index(Request $request) {

		// User Authentication + Role Authorization, we check to see if the user as the to access this method
		$request->user()->authorizeRoles(['superadmin', 'admin']);

		$users = User::all();
		return view('users.index', array(
			'users' => $users,
		));
	}
	
	public function show() {
		$courses = Course::all();
		return view('users.show', array('courses'=>$courses));
	}	
	
	public function getApproveUser() {
		$users = User::all();
		return view('users.approve-users', array('users'=>$users));
	}

	/**
	* @param string|array $roles
	*/
	public function authorizeRoles($roles)
	{
	if (is_array($roles)) {
		return $this->hasAnyRole($roles) || 
				abort(401, 'This action is unauthorized.');
	}
	return $this->hasRole($roles) || 
			abort(401, 'This action is unauthorized.');
	}
	/**
	* Check multiple roles
	* @param array $roles
	*/
	public function hasAnyRole($roles)
	{
	return null !== $this->roles()->whereIn(‘name’, $roles)->first();
	}
	/**
	* Check one role
	* @param string $role
	*/
	public function hasRole($role)
	{
	return null !== $this->roles()->where(‘name’, $role)->first();
	}

}