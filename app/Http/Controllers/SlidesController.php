<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Course as Course;
use App\Module as Module;
use App\Slides as Slides;
use App\Fileentry as Fileentry;
use App\User as User;
use App\Enrollment as Enrollment;

use Session;

class SlidesController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
	// protected from unauthorized users, checks if the current user is logged in. If the user is not logged in, they get redirected to the login page
	/**
	public function __construct() {
		$this->beforeFilter('auth');
		$this->beforeFilter('has_role_superadmin', array('only' => array('index','create', 'store', 'edit', 'update', 'destroy')));
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
 	*/
	protected $layout = "layouts.default";
	
	public function index()
	{
		// get all the slides
		$slides = Slides::all();

		// load the view and pass the slides
		return view('slides.index')->with('slides', $slides);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// load the create form (app/views/slides/create.blade.php)
		return view('slides.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		// validate
		$rules = array(
			'name'       => 'required',
			'menu_name'  => 'required'
		);

        $this->validate($request, $rules);


		// store
		$slide = new Slides;
		$slide->name      	 = $request->get('name');
		$slide->menu_name	 = $request->get('menu_name');
		$slide->description = $request->get('description');
		$slide->media_script = $request->get('media_script');
		$slide->transcript	 = $request->get('transcript');
		$slide->question = $request->get('question');
		$slide->answers = $request->get('answers');
		//dd($request->get('questions'));
		
		// check module_id is null/empy, if so set the foreign key to NULL
		$slide->module_id  = $request->get('module_id');
		if(! $request->get('module_id')){
			$slide->module_id  = null;
		}
		
		$slide->save();

		// upload image
		if($request->hasFile('image') != null){
			$file = $request->hasFile('image');
			$extension = $file->getClientOriginalExtension();
			$entry = new Fileentry();
			$entry->mime = $file->getClientMimeType();
			$entry->original_filename = $file->getClientOriginalName();
			$upload_folder = '/assets/uploads/';
			//move file
			$generalName =  'image_';
			$extension = $file->getClientOriginalExtension();
			$date = date("_d_m_Y_");  // getting the date
			$timeArray = gettimeofday();
			$time = $timeArray['sec'].$timeArray['usec'];  // getting the millesconds
			$newFileName = $generalName .'module_' . $slide->id . $date . $time .'.'. $extension; // keep original extension
			$entry->filename = $newFileName; // renameing image
			$file->move(public_path() . $upload_folder, $newFileName );
			$entry->slide_id = $slide->id;
			//dd($entry->original_filename);
			
			// resize image instance
			$path = public_path($upload_folder . $newFileName);
			// save image in desired format
			Image::make($path)->resize(400,200)->save($path);
			
			$entry->save();
		}else{
			$entry = new Fileentry();
			$entry->slide_id = $slide->id;
			$entry->save();
		}

		// save data
		if ($slide->save()) {
			// redirect
			Session::flash('message', 'Successfully created slide!');
			return Redirect('slides');
		} else {
			Session::flash('success', 'No record created.');
			return back()->withInput();

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// get all modules, then create slides_menu, which goes into nested second view "includes.sidarbar"
		$slides = Slides::find($id);
		$user = User::where('id','=', Auth::id())->first(); 
		$allModSlides = Slides::where('module_id', '=', $slides->module_id)->get();
		$allSlides = Course::find($slides->modules->course_id);
		$slidesCompleted = Enrollment::where('enrollment_status_id', '=', 3)->where('user_id', '=', Auth::id())->count(); 
		$question = json_decode($slides->question, true);
		$answers  = json_decode($slides->answers, true);

		//dd($slidesCompleted);
		//dd(Auth::id());
		 
		//check if the slides table has a relationship to modules table, if it's NULL then the sidebar view has a isset(modules_menu)
		if($slides->modules != null){
			$moduleid =  $slides->modules->course->id;
			$modules_menu = Course::with('modules.slides')->find($moduleid);
		}
		
		return view('slides.show', array(
			'slides'=> $slides,
			'allModSlides' => $allModSlides,
			'allSlides' => $allSlides,
			'slidesCompleted' => $slidesCompleted,
			'question' => $question,
			'answers' => $answers,
			'modules_menu' => $modules_menu,
			'user' => $user))->nest('child', 'includes.sidebar', compact('modules_menu'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// get the slide
		$slide = Slides::find($id);
		$answers = json_decode($slide->answers, true);

		// pull all courses from course table, assign to $module_list variable.
		$module_list = Course::pluck('name','id');
		// we prepend a default selection item
		$module_list->prepend('Select Module');
		// pull all courses from course table, assign to $course_list variable.
		$course_list = Course::pluck('name','id');
		// we prepend a default selection item
		$course_list->prepend('Select Course');

		// show the edit form and pass the slide
		return view('slides.edit', array(
			'slide' => $slide,
			'module_list' => $module_list,
			'course_list' => $course_list,
			'answers' => $answers

		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'name'       => 'required',
			'menu_name'  => 'required'
		);

        $this->validate($request, $rules);

		// update
		$slide = Slides::find($id);
		$slide->name      	 = $request->get('name');
		$slide->menu_name	 = $request->get('menu_name');
		$slide->description = $request->get('description');
		$slide->media_script = $request->get('media_script');
		$slide->transcript	 = $request->get('transcript');
		$slide->question = $request->get('question');
		$slide->answers = $request->get('answers');
		
		// check module_id is null/empy, if so set the foreign key to NULL
		$slide->module_id  = $request->get('module_id');

		if(! $request->get('module_id')){
			$slide->module_id  = null;
		}

		$slide->save();

		// upload image
		if($request->hasFile('image') != null){
			$file = $request->hasFile('image');
			$extension = $file->getClientOriginalExtension();
			$entry = Fileentry::where('slide_id' , '=', $id)->first();
			$entry->mime = $file->getClientMimeType();
			$entry->original_filename = $file->getClientOriginalName();
			$upload_folder = '/assets/uploads/';
			//move file
			$generalName =  'image_';
			$extension = $file->getClientOriginalExtension();
			$date = date("_d_m_Y_");  // getting the date
			$timeArray = gettimeofday();
			$time = $timeArray['sec'].$timeArray['usec'];  // getting the millesconds
			$newFileName = $generalName .'module_' . $slide->id . $date . $time .'.'. $extension; // keep original extension
			$entry->filename = $newFileName; // renameing image
			$file->move(public_path() . $upload_folder, $newFileName );
			$entry->slide_id = $slide->id;
			//dd($entry->original_filename);
			
			// resize image instance
			$path = public_path($upload_folder . $newFileName);
			// save image in desired format
			Image::make($path)->resize(400,200)->save($path);
			
			$entry->save();
		}
		
		// save data
		if ($slide->save()) {
			// redirect
			Session::flash('message', 'Successfully updated slide!');
			return Redirect('slides');
		} else {
			Session::flash('success', 'No record created.');
			return back()->withInput();

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$slide = Slides::find($id);
		$slide->delete();

		// redirect
		Session::flash('message', 'Successfully deleted the slide!');
		return Redirect('slides');
	}
}