<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	// protected from unauthorized users, checks if the current user is logged in. If the user is not logged in, they get redirected to the login page
	/**
	public function __construct() {
		$this->beforeFilter('auth');
		$this->beforeFilter('has_role_superadmin', array('only' => array('index','create', 'store', 'edit', 'update', 'destroy')));
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
 	*/

	protected $layout = "layouts.default";	 
	 
	public function index()
	{
		return Redirect('users/dashboard');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        // if the user doesn't exist
        if(!$user = User::find($id)){
            return View::make('pages/404');
        }	
		// inti variables
		$user = User::where('id','=', $id)->first();
		$userRole = User::where('id','=', Auth::id())->first(); 
		
		$this->layout->content = View::make('users.profile.show', array('user'=>$user, 'userRole' => $userRole));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
        // if the user doesn't exist
        if(!$user = User::find($id)){
            return View::make('pages/404');
        }
		
		// inti variables
		$user = User::where('id','=', $id)->first();
		$userRole = User::where('id','=', Auth::id())->first(); 
		
		// if user owns or has superadmin access to edit profile page
		if(Auth::id() == $id || $userRole->hasRole("superadmin") ){
		
			$this->layout->content = View::make('users.profile.edit', array('user'=>$user, 'userRole' => $userRole));
					
		}else{
			return 'You don\'t have access for that!';
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $data = Input::all();

        $user = User::find($id);

       /*  $validator = User::validator($data, $id);

        if($validator->fails()){
            return Redirect::back()->withInput(Input::except('_method'))->withErrors($validator);
        } */

        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->email = Input::get('email');
        $user->approval = Input::get('approval');
        $user->save();
		
		return Redirect::to('/profile/'.$id)->with('message', 'Profile updated');	
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$user = User::find($id);
		$user->delete();

		// redirect
		Session::flash('message', 'Successfully deleted the user!');
		return Redirect::to('/users/approve-user');
	}
}
