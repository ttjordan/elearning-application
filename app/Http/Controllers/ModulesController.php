<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Course as Course;
use App\Module as Module;

use Session;

class ModulesController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
	// protected from unauthorized users, checks if the current user is logged in. If the user is not logged in, they get redirected to the login page
	/**
	public function __construct() {
		$this->beforeFilter('auth');
		$this->beforeFilter('has_role_superadmin', array('only' => array('index','create', 'store', 'edit', 'update', 'destroy')));
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
 	*/

	protected $layout = "layouts.default";
	// check BaseModel for logic. Before save, check if value is nullable. Often a foreign key needs to be set to NULL
	protected $nullable = ['course_id', 'module_id']; 
	
	public function index()
	{
		// get all the modules
		$modules = Module::all();

		// load the view and pass the modules
		return view('modules.index')->with('modules', $modules);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// load the create form (app/views/modules/create.blade.php)
		return view('modules.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		// validate
		$rules = array(
			'name'       => 'required',
			'menu_name'       => 'required'
		);

        $this->validate($request, $rules);

		// store
		$module = new Module;
		$module->name       =  $request->get('name');
		$module->menu_name  =  $request->get('menu_name');
		$module->course_id  =  $request->get('course_id');
		//$module->description       =  $request->get('description');
		// check if course_id is null/empy, if so set the foreign key to NULL
		if(!  $request->get('course_id')){
			$module->course_id  = null;
		}
		
		$module->save();
			
		//dd(Input::file('image'));
		
		// upload image
		/*
		$file = Input::file('image');
		$extension = $file->getClientOriginalExtension();
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$upload_folder = '/assets/uploads/';
		//move file
		$generalName =  'image_';
		$extension = $file->getClientOriginalExtension();
		$date = date("_d_m_Y_");  // getting the date
		$timeArray = gettimeofday();
		$time = $timeArray['sec'].$timeArray['usec'];  // getting the millesconds
		$newFileName = $generalName .'module_' . $module->id . $date . $time .'.'. $extension; // keep original extension
		$entry->filename = $newFileName; // renameing image
		$file->move(public_path() . $upload_folder, $newFileName );
		$entry->module_id = $module->id;
		//dd($entry->original_filename);
		
		// resize image instance
		$path = public_path($upload_folder . $newFileName);
		// save image in desired format
		Image::make($path)->resize(400,200)->save($path);
		
		$entry->save();
		*/

		// save data
		if ($module->save()) {
			// redirect
			Session::flash('message', 'Successfully created module!');
			return Redirect('modules');
		} else {
			Session::flash('success', 'No record created.');
			return back()->withInput();

		}

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// show the view and pass the module to it
		$modules = Module::find($id);

		//check if the modules table as relationship to course table, if it's NULL then the sidebar view has a isset(modules_menu)
		if($modules->course != null){
			$courseid = $modules->course->id;
			$modules_menu = Course::with('modules.slides')->find($courseid);
		}
		
		return view('modules.show', array('modules'=>$modules))->nest('child', 'includes.sidebar', compact('modules_menu'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// get the module
		$module = Module::find($id);


		// pull all courses from course table, assign to $course_list variable.
		$course_list = Course::pluck('name','id');
		// we prepend a default selection item
		$course_list->prepend('Select Course');

		// show the edit form and pass the module
		return view('modules.edit', array(
			'module' => $module,
			'course_list' => $course_list
		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		// validate
		$rules = [
			'name'       => 'required',
			'menu_name'      => 'required'
		];

        $this->validate($request, $rules);

		// process the login

		// store
		$module = Module::find($id);

		$module->name       =  $request->get('name');
		$module->menu_name     =  $request->get('menu_name');
		$module->course_id  =  $request->get('course_id');
		//$module->description     =  $request->get('description');
		// check if course_id is null/empy, if so set the foreign key to NULL
		if(!$request->get('course_id')){
			$module->course_id  = null;
		}

		$module->save();
		
		// upload image
		/*
		$file = Input::file('image');
		$extension = $file->getClientOriginalExtension();
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$upload_folder = '/assets/uploads/';
		//move file
		$generalName =  'image_';
		$extension = $file->getClientOriginalExtension();
		$date = date("_d_m_Y_");  // getting the date
		$timeArray = gettimeofday();
		$time = $timeArray['sec'].$timeArray['usec'];  // getting the millesconds
		$newFileName = $generalName .'module_' . $module->id . $date . $time .'.'. $extension; // keep original extension
		$entry->filename = $newFileName; // renameing image
		$file->move(public_path() . $upload_folder, $newFileName );
		$entry->module_id = $module->id;
		//dd($entry->original_filename);

		// resize image instance
		$path = public_path($upload_folder . $newFileName);
		// save image in desired format
		Image::make($path)->resize(400,200)->save($path);
		
		$entry->save();
		*/

		// save data
		if ($module->save()) {
			// redirect
			Session::flash('message', 'Successfully updated module!');
			return Redirect('modules');
		} else {
			Session::flash('success', 'No record created.');
			return back()->withInput();

		}
	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// delete
		$module = Module::find($id);
		$module->delete();

		// redirect
		Session::flash('message', 'Successfully deleted the module!');
		return Redirect('modules');
	}

}