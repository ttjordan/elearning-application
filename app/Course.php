<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

	protected $table = 'courses';
	public $timestamps = true;

	public function modules()
	{
		return $this->hasMany('App\Module');
	}	
	
   /**
     * Get all of the posts for the country.
     */
    public function slides()
    {
        return $this->hasManyThrough('App\Slides', 'App\Module');
    }

    

}