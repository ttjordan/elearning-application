<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Fileentry extends Model {
	
	public function module()
	{
		return $this->belongsTo('Module', 'module_id');
	}
	
	public function slide()
	{
		return $this->belongsTo('Slides', 'slide_id');
	}
}