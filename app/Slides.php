<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slides extends Model {

	protected $table = 'slides';
	public $timestamps = false;

	public function modules()
	{
		return $this->belongsTo('App\Module', 'module_id');
	}
	
	public function fileentry()
	{
		return $this->hasOne('App\Fileentry', 'slide_id');
	}
}