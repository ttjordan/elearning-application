<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

	protected $table = 'modules';
	public $timestamps = true;

	public function course()
	{
		return $this->belongsTo('App\Course', 'course_id');
	}
	
	public function enrollment()
	{
		return $this->hasMany('App\Enrollment');
	}
	
	public function slides()
	{
		return $this->hasMany('App\Slides');
	}

	public function fileentry()
	{
		return $this->hasOne('App\Fileentry');
	}
	
	public static function sectionsCountRelation($id)
	{
	 // return  DB::table('modules')->select(DB::raw('module_id, count(*) as count')->where('module_id', '=', 1)->groupBy('module_id'));

	  return DB::select('select * from modules where course_id = '. $id);
	}
}